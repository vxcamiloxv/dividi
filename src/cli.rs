//  Dividi - an automation tool and media library management.
// Copyright (C) 2023 Camilo Q.S. (Distopico) <distopico@riseup.net>
//
// This file is part of Dividi.
//
// Dividi is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::path::PathBuf;

use clap::{Parser, Subcommand, Args};

/// Automation tool and media library management.
#[derive(Parser, Debug)]
#[command(author, about, version)]
pub struct DividiCli {
    /// Alternative configuration file [default:
    /// $XDG_CONFIG_HOME/dividi/dividi.toml].
    #[arg(short = 'c', long)]
    config: Option<PathBuf>,

    #[command(subcommand)]
    subcommand: SubCommands,
}

impl DividiCli {
    pub fn new() {
        let args = Self::parse();

        match args.subcommand {
            SubCommands::Run(args) => run_command(args),
        }
    }
}

#[derive(Subcommand, Debug)]
pub enum SubCommands {
    Run(RunArgs),
}

#[derive(Args, Debug)]
pub struct RunArgs {
    #[arg(short, long)]
    task: Option<String>,
}

fn run_command(args: RunArgs) {
    println!("Run!! {:?}", args.task);
}
